let data = {
    site: {
        common: {
            name: 'App Booster',
            slogan: '',
            logo: 'logo.svg'
        },
        pages: {
            defaults: {
                title: 'Page title',
                description: 'Description page',
                useSocialMetaTags: false,
                name: 'Defaults',
                value: 'defaults'
            },
            index: {
                title: 'Мои приложения',
                description: 'Description page',
                action: true,
                useSocialMetaTags: false,
                name: 'index',
                value: 'index'
            },
            profile: {
                title: 'Профиль',
                description: 'Description page',
                useSocialMetaTags: false,
                name: 'profile',
                value: 'profile'
            },
            balance: {
                title: 'Баланс',
                description: 'Description page',
                useSocialMetaTags: false,
                name: 'balance',
                value: 'balance'
            },
            competitors: {
                title: 'Конкуренты',
                description: 'Description page',
                useSocialMetaTags: false,
                name: 'competitors',
                value: 'competitors'
            },
            order1: {
                title: 'Заявка на корректировку рейтинга',
                icon: 'chevron',
                description: 'Description page',
                useSocialMetaTags: false,
                name: 'order',
                value: 'order'
            },
            order2: {
                title: 'Заявка на вывод в ТОП',
                icon: 'chevron',
                description: 'Description page',
                useSocialMetaTags: false,
                name: 'order-2',
                value: 'order-2'
            },
            order3: {
                title: 'Выбор приложения',
                icon: 'chevron',
                description: 'Description page',
                useSocialMetaTags: false,
                name: 'order-3',
                value: 'order-3'
            },
        },
        nav: {
            primary: {
                app: {
                    icon: 'apps',
                    name: 'Мои приложения',
                    value: 'index',
                    child: false
                },
                campains: {
                    icon: 'campains',
                    name: 'Кампании',
                    value: 'campains',
                    child: false
                },
                analytics: {
                    icon: 'analytics',
                    name: 'Аналитика',
                    value: 'analytics',
                    child: {
                        trendingSearches: {
                            icon: false,
                            name: 'Trending searches',
                            value: 'trending-searches'
                        },
                        appPosition: {
                            icon: false,
                            name: 'Позиции приложений',
                            value: 'app-position'
                        },
                        competitors: {
                            icon: false,
                            name: 'Конкуренты',
                            value: 'competitors'
                        },
                    }
                },
                faq: {
                    icon: 'faq',
                    name: 'FAQ',
                    value: 'faq',
                    child: false
                }
            },
            secondary: {
                app: {
                    icon: false,
                    name: 'Блог',
                    value: 'blog',
                    child: false
                },
                requisites: {
                    icon: false,
                    name: 'Реквизиты',
                    value: 'requisites',
                    child: false
                },
                rules: {
                    icon: false,
                    name: 'Правила',
                    value: 'rules',
                    child: false
                }
            }
        }
    }
};
