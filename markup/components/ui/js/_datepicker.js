import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
// import moment from 'moment';

// ReactDOM.render(
//     <React.Fragment>
//         <DatePicker
//             className="form__control form__control--date"
//         />
//     </React.Fragment>
//     , document.getElementById('datePicker'))


/* <DatePicker
  selected={this.state.startDate}
  onChange={this.handleChange}
  className="form__control form__control--date"
/> */

ReactDOM.render(
    React.createElement(
        React.Fragment,
        null,
        React.createElement(DatePicker, {
            className: "form__control form__control--date"
        })
    )
    , document.getElementById('datePicker')
);
