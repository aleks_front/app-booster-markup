import React from 'react';
import ReactDOM from 'react-dom';
import Slider from 'rc-slider';

// ReactDOM.render(
//     <React.Fragment>
//         <Slider />
//     </React.Fragment>
//     , document.getElementById('formSlider'));
// );

ReactDOM.render(
    React.createElement(
        React.Fragment,
        null,
        React.createElement(Slider)
    )
    , document.getElementById('formSlider')
);
