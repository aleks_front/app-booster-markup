module.exports = {
    'postcss': [
        {
            'name': 'postcss-write-svg'
        },
        {
            'name': 'postcss-inline-svg'
        },
        {
            'name': 'postcss-brand-colors'
        },
        {
            'name': 'postcss-flexbugs-fixes'
        },
        {
            'name': 'css-mqpacker',
            'options': {
                'sort': true
            }
        }
    ],
    'svg': {
        'active': true,
        'workflow': 'symbols',
        'symbolsConfig': {
            'loadingType': 'inject',
            'usePolyfillForExternalSymbols': true,
            'pathToExternalSymbolsFile': ''
        }
    },
    'css': {
        'workflow': 'manual'
    },
    'js': {
        'workflow': 'modular',
        'bundler': 'webpack',
        'lint': true,
        'useBabel': true,
        'removeConsoleLog': true,
        'webpack': {
            'useHMR': false,
            'providePlugin': {}
        },
        'jsPathsToConcatBeforeModulesJs': [],
        'lintJsCodeBeforeModules': false,
        'jsPathsToConcatAfterModulesJs': [],
        'lintJsCodeAfterModules': false
    },
    'sourcemaps': {
        'js': {
            'active': true,
            'inline': true
        },
        'css': {
            'active': true,
            'inline': true
        }
    },
    'notifyConfig': {
        'useNotify': false,
        'title': 'TARS notification',
        'sounds': {},
        'taskFinishedText': 'Task finished at: '
    },
    'minifyHtml': false,
    'generateStaticPath': true,
    'buildPath': './builds/',
    'useBuildVersioning': false,
    'useArchiver': true,
    'ulimit': 4096,
    'templater': 'pug',
    'cssPreprocessor': 'scss',
    'useImagesForDisplayWithDpi': [
        96,
        192,
        288,
        384
    ],
    'fs': {
        'staticFolderName': 'static',
        'imagesFolderName': 'img',
        'componentsFolderName': 'components'
    },
    'staticPrefix': 'static/'
};
